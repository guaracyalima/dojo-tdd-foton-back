package com.github.guaracyalima.cd;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class IntegracaoTest {

	@Test
	void testName() throws Exception {
		System.setProperty("webdriver.chrome.driver", "/home/araujo/sdk/chromedriver");
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		driver.manage().window().maximize();
		driver.get("http://localhost:4200");

		// navega para a url de vendedores

		driver.findElement(By.xpath("//*[@id=\"linkvendedores\"]")).click();

		int size = driver.findElements(By.xpath("//*[@id=\"pr_id_1\"]/div/table/tbody/tr")).size();

		assertEquals(5, size);

		driver.close();
	}
}
