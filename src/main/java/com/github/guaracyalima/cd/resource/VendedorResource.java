package com.github.guaracyalima.cd.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.github.guaracyalima.cd.model.Vendedor;
import com.github.guaracyalima.cd.services.VendedorService;

@RestController
@RequestMapping("/vendedor")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class VendedorResource {

	@Autowired
	private VendedorService service;
	
//	@GetMapping(produces=MediaType.APPLICATION_JSON_VALUE)
//	@ResponseBody
//	public ResponseEntity<String> ping() {
//		return ResponseEntity.ok().body("Pong");
//	}
	
	@GetMapping
	@ResponseBody
	public ResponseEntity<List<Vendedor>> index() {
		return new ResponseEntity<List<Vendedor>>(HttpStatus.OK).ok(service.index());
	}
}
