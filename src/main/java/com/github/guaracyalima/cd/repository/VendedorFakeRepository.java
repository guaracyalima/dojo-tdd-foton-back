package com.github.guaracyalima.cd.repository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.github.guaracyalima.cd.model.Vendedor;

@Service
public class VendedorFakeRepository {

	public List<Vendedor> index(){
		List<Vendedor> list = new ArrayList<Vendedor>();
		
		Vendedor v1 = Vendedor.builder().nome("Araujo").comissao(new BigDecimal(15)).build();
		Vendedor v2 = Vendedor.builder().nome("Jacomé").comissao(new BigDecimal(5)).build();
		Vendedor v3 = Vendedor.builder().nome("Tranca").comissao(new BigDecimal(10)).build();
		Vendedor v4 = Vendedor.builder().nome("Maciel").comissao(new BigDecimal(3)).build();
		Vendedor v5 = Vendedor.builder().nome("Silva").comissao(new BigDecimal(1)).build();
		
		list.add(v1);
		list.add(v2);
		list.add(v3);
		list.add(v4);
		list.add(v5);
		
		return list;
	}
	
}
