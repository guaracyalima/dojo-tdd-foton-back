package com.github.guaracyalima.cd.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.guaracyalima.cd.model.Vendedor;
import com.github.guaracyalima.cd.repository.VendedorFakeRepository;

@Service
public class VendedorService {

	@Autowired
	private VendedorFakeRepository repository;
	
	public List<Vendedor> index() {
		return repository.index();
	}
}
